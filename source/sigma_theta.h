#ifdef cplusplus
extern "C" {
#endif


#define ST_VERSION "4.0"
#define ST_DATE "2019/06/22"
static const char st_version[]=ST_VERSION;
static const char st_date[]=ST_DATE;


/**
 * \enum    st_dev_type
 * \brief   Deviation type.
 * \details Used to specify the deviation type returned by serie_dev().
 */
typedef enum
    {
        ADEV,   /*!< Allan deviation */
        MDEV,   /*!< Modified Allan deviation */
        PDEV,   /*!< Parabolic deviation */
        HDEV,   /*!< Hadamard deviation */
        GCOV    /*!< Grolambert covariance */
    }
    st_dev_type;

/**
 * \typedef  st_asymptote_coeff
 * \brief    Array containing the tau^-1, tau^-1/2, tau^0, tau^1/2 and tau
 *           asymptote coefficients of a sequence of serie deviation.
 */
typedef double st_asymptote_coeff[6];

/**
 * \struct st_conf_int
 * \brief  95 % (2 sigma) and 68% (1 sigma) confidence intervals data structure
 */
typedef struct st_conf_int
{
    double bmin2s;       /*!< 95 % confidence low value */
    double bmin1s;       /*!< 68 % confidence low value */
    double bmax1s;       /*!< 68 % confidence high value */
    double bmax2s;       /*!< 95 % confidence high value */
} st_conf_int;

/**
 * \struct st_serie
 */
typedef struct st_serie
{
    double tau[128];            /*!< Tau serie */
    double dev[128];            /*!< Deviation estimate */
    double dev_unb[128];        /*!< Unbiased estimate */
    int alpha[128];             /*!< Dominating power law */
    st_asymptote_coeff asym;    /*!< Asymptotes coefficients */
    st_conf_int conf_int[128];  /*!< Confidence intervals data structure */
    size_t length;              /*!< Length of serie */
} st_serie;

/**
 * \struct st_tau_inc
 */
typedef struct st_tau_inc
{
    unsigned int tau[10];     /*!< "Pattern" list of Tau value */
    size_t length;      /*!< Length of Tau list */
} st_tau_inc;

/**
 * \struct st_psd
 */
typedef struct st_psd
{
    double* ff;     /*!< Fourrier frequency values array */
    double* syy;    /*!< PSD values array */
    size_t length;  /*!< Length of array */
} st_psd;

typedef struct stio_flag {
    char graph;
    char conf;
    char bias;
    char title;
    char fit;
    char asymptote;
    char slopes[6];
    char display;
    char log_inc;
    double log;
    st_tau_inc ortau;
    char variance;
} stio_flag;

typedef struct filter_coef
{
    double hm3;
    double hm2;
    double hm1;
    double h0;
    double hp1;
    double hp2;
    double C1;
    double C0;
} filter_coef;


/**
 * \brief           Compute the deviation serie of a normalized frequency
 *                  deviation sequence.
 * \details         Deviation serie (ADEV, MDEV, PDEV or HDEV) of a normalized
 *                  frequency deviation sequence from tau=tau0 (tau0=sampling
 *                  step) to tau=N*tau0/2 or N*tau0/3 (N=number of samples)
 *                  according to the variance type (dev_type) by octave
 *                  (log step: tau_n+1=2*tau_n, default setting).
 *
 * \param *serie    Pointer to st_serie data structure.
 * \param ortau     Tau increment structure.
 * \param dev_typ   Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param n         Length of array
 * \param tau       Integration time in second
 * \param *y        Array of sample value.
 * \return          0 in case of successful completion.
 */
int st_serie_dev(st_serie *, st_tau_inc, int, size_t, double, double *, ...);

/**
 * \brief    Fit data to polynom ?
 * \details  Recherche des coefficients du polynome passant par les points M
 *           (donnee[0][i],donnee[1][i],donnee[2][i]) par une methode derivee
 *           des moindres carres (somme des incertitudes relatives minimum). Weighted Least Squares
 * \param    serie
 * \param    war          Weight (one over square of variance)
 * \param    ord
 * \param    flag_slopes
 * \return   Error code
 */
int st_relatfit(st_serie *, double *, int, unsigned char *);

/**
 * \brief
 *
 * \param t
 * \param intyp
 * \return
 */
double st_interpo(double, int);

/**
 */
int st_asym2alpha(st_serie *, int);

/**
 * \brief          Computation of the degrees of freedom of the Allan variance
 *                 estimates.
 * \details        Based on "Uncertainty of stability variances based on finite
 *                 differences" by C.A.Greenhall and W.J.Riley, 35th PTTI).
 * \param serie    Pointer to st_serie data structure.
 * \param edf      Array of equivalent degree of freedom.
 * \param flag_variance  Variance type.
 * \return         0 in case of of successful completion.
 */
int st_avardof(st_serie *, double *, char);

/**
 * \brief         Computes the 95 % (2 sigma) and the 68% (1 sigma) confidence
 *                intervals of a sequence of Allan Deviations.
 * \param *serie  Pointer to st_serie data structure.
 * \param edf     Array of equivalent degree of freedom.
 * \return        0 in case of of successful completion.
 */
int st_aduncert(st_serie *, double *);

/**
 * \brief       Power Spectral Density (PSD) calculation.
 * \details     Compute the Power Spectral Density of the 'ny' normalized
 *              frequency deviation elements of the vector '*y' (frequency deviation) 
 *              versus the Fourrier frequency.
 * \param *psd  Structure containing PSD data (ff, Syy, size of data).
 * \param *t    Array of timetag data.
 * \param *y    Array of normalized frequency deviation samples.
 * \param ny    Number of elements of samples used in the computation.
 * \param idec  Decimation flag (0 no decimation else decimation).
 * \return      0 in case of successful completion.
 */
int st_psdgraph(st_psd *, double *, double *, size_t, int);


typedef struct conf_int
{
    double mean;       /*   */
    double inf_bound;  /*   */
    double sup_bound;  /*   */
} conf_int;

conf_int st_raylconfint(double);

conf_int st_raylconfint1s(double);

double st_tchebyfit(long, double *, double *);


long *st_matlab_sort(double *, size_t, double *, long *, long *);
double st_pgaussdirectfin(double *, double *, double, double);
double st_pgaussdirectavecestimbr(double *, double, double, double, double, int, double);
double *st_eig(double *, size_t, double *, double *);
double st_xinvy(double *, double *, double, size_t);
//int st_ci_kltgs(double *, double, double, int, double (*)[3], double (*)[5], long (*)[3], double (*)[3], double (*)[3], double (*)[3], double *, long *);
int st_ci_kltgs(double mest[3], double varnoise, double ddl, int klt, double est[3][3], double ci[3][5], long *indices[3], double *montecarl[3], double *asort[3], double *pmont[3], double *cd, long *indix);


/*	Subroutines for generating random noises.			     */
/*						FV	1989/02/21	     */

/* Initialization of the random number generator using /dev/urandom	     */
long stu_init_rnd();

/**
 * \brief          Generation of a sequence of 'nbr_dat' Gaussian random
 *                 numbers, centered and with unity rms.
 * \param nbr_dat  Length of the sequence.
 * \param *x       Array containing sequence.
 * \return         Estimator of the RMS of the computed sequence.
 */
double stu_gausseq(long unsigned int , double *);

/*  Subroutine filtering the white noise sequence of 'nbr_dat' Gaussian     */
/*  data sampled with a step 'tau0' in order to obtain a sequence with      */
/*  a linear frequency drift and a Power Spectral Density following power   */
/*  laws (from f^{-2} to f^{+2}) according to the entered coefficients.     */
/*									    */
/*							    1994/02/02	    */
/*									    */
/*						    Francois Vernotte	    */
/*									    */
void stu_filtreur(int, double *, double, filter_coef);

/* Conversion of a frequency deviation Yk sequence into a time error x(t) sequence.   */
/* The arbitrarily convention x(t0) = Y0 is used.                                     */
/* At the beginning the YK are in x[], at the end the x(t) are also in x[].           */
void stu_yk_xt(long, double *, double, double);

/*  Adding the suffix to the file name.					    */
/*							    FV	1992/04/17  */
char *stu_mod_nom(char *,char*);


/**
 * \brief           Generate the real list of Tau.
 * \param *serie    Pointer to st_serie data structure.
 * \param dev_type  Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param ortau     Tau increment structure pattern.
 *                  Choose 2 for increment by octave
 *                  (default setting), 10 for increment by decade, etc. If 1 is
 *                  selected, increasing values are expected, e.g. 'Tau: 1 2 5'.

 * \param n         Number of value.
 * \return          0 in case of succefull completion.
 */
int stio_gen_tau_list(st_serie *, int, st_tau_inc, size_t);

/**
 * \brief          Load the file pointed by 'source' and transfer its content
 *                 into the 't' and 'y' tables.
 * \param *source  Path to data file.
 * \param **t      Pointer to array of timetag.
 * \param **y      Pointer to array of sample value.
 * \return         -1 file not found,
 *                 0 unrecognized file,
 *                 N length of the tables.
 */
long stio_load_ykt(char *, double **, double **);

/**
 * \brief           Load the two files pointed by 'source1' and 'source2' and
 *                  transfer its content into the 't' and 'y' tables.
 * \param *source1  Path to data file 1.
 * \param *source2  Path to data file 2.
 * \param **t       Pointer to array of timetag.
 * \param **y1      Pointer to array of sample value.
 * \param **y2      Pointer to array of sample value.
 * \return          -1 file not found,
 *                  0 unrecognized file,
 *                  N length of the tables.
 */
long stio_load_2yk(char *, char *, double **, double **, double **);

/**
 * \brief           Load the two files pointed by 'source1' and 'source2' and
 *                  transfer its content into the 't' and 'y' tables.
 * \param *source1  Path to data file 1.
 * \param *source2  Path to data file 2.
 * \param *source3  Path to data file 3.
 * \param **t       Pointer to array of timetag.
 * \param **y12     Pointer to array of sample value.
 * \param **y23     Pointer to array of sample value.
 * \param **y31     Pointer to array of sample value.
 * \return          -1 file not found,
 *                  0 unrecognized file,
 *                  N length of the tables.
 */
long stio_load_3yk(char *, char *, char *, double **, double **, double **, double **);


/**
 * \brief          Load the two files pointed by 'source' and transfer
 *                 its content into the 'y' table.
 * \param *source  Path to data file.
 * \param **y      Pointer to array of sample value.
 * \return         -2 file with 2 columns
 *                 -1 file not found,
 *                 0 unrecognized file,
 *                 N length of the tables.
 */
long stio_load_1col(char *, double **);


/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'tau' and 'adev' tables.
 * \param *source
 * \param tau[]
 * \param adev[]
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_adev(char *source, double tau[], double adev[]);


/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'coeff' tables.
 * \param *source
 * \param coef
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_coef(char *, st_asymptote_coeff);


/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'tau', 'adev' and 'ubad' tables.
 * \param *source
 * \param tau[]
 * \param adev[]
 * \param ubad[]
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_3col(char *, double *, double *, double *);


/**
 * \brief          Load the file pointed by 'source' and transfer its contain
 *                 into the 'tau', 'adev', 'ubad' and bounds (b1, b2, b3, b4)
 *                 tables.
 * \param *source
 * \param tau[]
 * \param adev[]
 * \param ubad[]
 * \param b1[]
 * \param b2[]
 * \param b3[]
 * \param b4[]
 * \return         -1 file not found
 *                 0 unrecognized file
 *                 N length of the tables
 */
int stio_load_7col(char *source, double tau[], double adev[], double ubad[], double b1[], double b2[], double b3[], double b4[]);


int stio_gener_gplt(char *outfile, int N, double tau[], double adev[], double bmax[], char *est_typ, st_asymptote_coeff coeff, struct stio_flag);

int stio_gen_psdplt(char *outfile, int N, double freq[], double syf[], struct stio_flag);

int stio_gen_linplt(char *outfile, int N, double tt[], double xy[], int xory, struct stio_flag);

int stio_gen_gcodplt(char *outfile, char names[][256], int N, int nbf, double tau[], double gcod[][256], int ind_gcod, struct stio_flag);

int stio_gen_3chplt(char input[][256], char *outfile, int N, double tau[], double gcod[][256], double bmax[][256], int flagest, struct stio_flag);


/**
 * \brief       Initialize flags by reading the configuration file
 *              $HOME/.SigmaTheta.conf
 * \param flag  Structure containing flags
 * \return      0 if method complete without error
 */
int stio_init_flag(struct stio_flag *);


/**
 * PRIVATE
 */

double cdf_rayleigh(double , double);
double dcdfr(double, double);


#ifdef cplusplus
}
#endif
