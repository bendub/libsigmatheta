/*   dev_sbr.c                                     F. Vernotte - 2014/10/02 */
/*                               From adev_sbr.c, created by FV, 2010/10/20 */
/*                               From mdev_sbr.c, created by FV, 2014/09/24 */
/*		     Adding of Parabolic deviation (PDev) by FV, 2015/02/06 */
/*       Subroutines for the computation of several Deviations (ADEV, MDev) */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systoms and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "sigma_theta.h"


#define ullong "unsigned long long"

/**
 * \brief       Allan deviation.
 * \details     Compute the Allan deviation of the 'ny' frequency deviation
 *              elements of the vector '*y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double adev_y(double *y, int tau, size_t ny)
{
    size_t i, nt;
    double Myd, Myf, al;

    nt = ny - (size_t)(2*tau + 1);
    Myd = Myf = (double)0;
    for (i=0; i<(size_t)tau; ++i)
        {
            Myd += *(y+i);
            Myf += *(y+i+tau);
        }
    if (nt == 1)
        al = sqrt((double)2) * fabs(Myd-Myf) / ((double)ny);
    else
        {
            al = pow(Myf-Myd, ((double)2));
            for (i=1; i<nt; ++i)
                {
                    Myd += *(y+tau+i-1) - *(y+i-1);
                    Myf += *(y+2*tau+i-1) - *(y+tau+i-1);
                    al += pow(Myf-Myd, ((double)2));
                }
            al = sqrt(al) / (((double)tau) * sqrt((double)(2*nt)));
        }
    return(al);
}


/**
 * \brief       Groslambert codeviation
 * \details     gcodev_y(tau,ny) : compute the Groslambert codeviation (see arXiv:1904.05849) of the 'ny' frequency deviation elements of the vectors 'y1' and 'y2' with an integration time 'tau'.
 * \param  *y   Array of sample value
 * \param  tau  Integration time
 * \param  ny   Number of elements of samples used in the computation
 */
double gcodev_y(double *y1, double *y2, int tau, size_t ny)
{
    int i,nt;
    double Myd1,Myf1,Myd2,Myf2,al,bl;

    nt=(int)ny-2*tau+1;
    Myd1=Myf1=Myd2=Myf2=(double)0;
    for (i=0;i<tau;++i)
	{
            Myd1+=*(y1+i);
            Myf1+=*(y1+i+tau);
            Myd2+=*(y2+i);
            Myf2+=*(y2+i+tau);
	}
    if (nt==1)
	{
            al=(Myd1-Myf1)*(Myd2-Myf2);
            if (al>0)
		bl=sqrt( ((double)2) * al ) / ((double)ny);
            else
		bl=-sqrt( ((double)-2) * al ) / ((double)ny);
	}
    else
        {
            al=(Myf1-Myd1)*(Myf2-Myd2);
            for(i=1;i<nt;++i)
                {
                    Myd1+=*(y1+tau+i-1)-*(y1+i-1);
                    Myf1+=*(y1+2*tau+i-1)-*(y1+tau+i-1);
                    Myd2+=*(y2+tau+i-1)-*(y2+i-1);
                    Myf2+=*(y2+2*tau+i-1)-*(y2+tau+i-1);
                    al+=(Myf1-Myd1)*(Myf2-Myd2);
                }
            if (al>0)
                bl=sqrt(al)/(((double)tau)*sqrt((double)(2*nt)));
            else
                bl=-sqrt(-al)/(((double)tau)*sqrt((double)(2*nt)));
        }
    //    if (isnan(bl)) printf("Myd1=%12.6le, Myf1=%12.6le, Myd2=%12.6le, Myf2=%12.6le, al=%12.6le, ny=%d, nt=%d\n",Myd1,Myf1,Myd2,Myf2,al,ny,nt);
    return(bl);
}


/**
 * \brief       Dudullized modified Allan deviation (Francois Meyer, 199?).
 * \details     Compute the modified Allan deviation of the 'ny' frequency
 *              deviation elements of the vector 'y' with an integration time
 *              'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double mdev_y(double *y, int tau, size_t ny)
{
    int i, j, k, l, to2, to3m1, to2m1, tom1;
    double result, moypon, coco, qto_ajto;

    moypon = 0;
    to2 = 2*tau;

    l = 3*tau-2;
    for (i=0; i<tau; ++i)
        moypon += (i+1) * (*(y+i)-*(y+l-i));

    k = tau-2;
    l = 2*tau-1;
    for (i=tau; i<l; ++i)
        {
            moypon += k * *(y+i);
            k-=2;
        }

    result = moypon*moypon;

    qto_ajto = 0;

    for (j=0; j<tau; ++j)
        qto_ajto += -*(y+j+to2) - *(y+j);

    for (j=tau; j<to2; ++j)
        qto_ajto += 2 * *(y+j);

    l = (int)ny - tau*3;
    to3m1 = 3*tau - 1;
    to2m1 = 2*tau - 1;
    tom1 = tau - 1;

    for(i=1; i<l; ++i)
        {
            moypon += qto_ajto;
            result += moypon*moypon;
            qto_ajto += *(y+i-1) + 3*(*(y+i+to2m1)-*(y+i+tom1))-*(y+i+to3m1);
        }

    coco = (double)tau;
    coco *= coco;
    coco *= coco;
    result /= ((double)2) * coco * ((double)((int)ny-3*tau+1));
    return(sqrt(result));
}

/**
 * \brief       Hadamard deviation.
 * \details     Compute the Hadamard deviation of the 'ny' frequency deviation
 *              elements of the vector 'y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double hadamard_y(double *y, int tau, size_t ny)
{
    int i, ifin, cpi;
    double y1, y2, y3, dy, dify, tau2;

    tau2 = tau * tau;
    if ((tau<1) || (tau>(int)(ny/3))) dify=0;
    else
        {
            y1 = y2 = y3 = (double)0;
            for (i=0; i<tau; ++i)
                {
                    y1 += (double)*(y+i);
                    y2 += (double)*(y+i+tau);
                    y3 += (double)*(y+i+2*tau);
                }
            ifin = (int)ny - 2*tau;
            dify = 0;
            cpi = 1;
            if (tau < (int)(ny/3))
                {
                    for(i=tau; i<ifin; ++i)
                        {
                            ++cpi;
                            dy = ((double)2)*y2 - y1 - y3;
                            dify += dy * dy / tau2;
                            y1 -= (double)*(y+i-tau);
                            y1 += (double)*(y+i);
                            y2 -= (double)*(y+i);
                            y2 += (double)*(y+i+tau);
                            y3 -= (double)*(y+i+tau);
                            y3 += (double)*(y+i+2*tau);
                        }
                    dy = ((double)2) * y2 - y1 - y3;
                    dify += dy * dy / tau2;
                    dify /= (double)(cpi);
                }
            else
                dify = (((double)2)*y2-y1-y3) * (((double)2)*y2-y1-y3) / tau2;
            dify /= (double)9;
        }
    return(sqrt(dify));
}


/**
 * \brief       Parabolic deviation (Vernotte et al. 2015, arXiv:1506.00687).
 * \details     Compute the Parabolic deviation of the 'ny' frequency deviation
 *              elements of the vector 'y' with an integration time 'tau'.
 *
 * \param  *y   Array of sample value
 * \param  tau  Integration time to be computed
 * \param  ny   Number of elements of samples used in the computation
 */
double pdev_y(double *y, int tau, size_t ny)
{
    double *H0, OV, Ovi;
    int dtmx, Tm, Tp, *Tc, bb, fin, ja, i;
    size_t ia, N;
    assert(tau>0);
    dtmx = 2*(int)tau - 2;
    H0 = malloc((unsigned int)dtmx*sizeof(double));
    Tc = malloc((unsigned int)dtmx*sizeof(int));
    //H0 = (double *)malloc(dtmx*sizeof(double));
    //Tc = (int *)malloc(dtmx*sizeof(int));
    if(tau == 1)
        {
            Tc[0] = 0;
            Tc[1] = 1;
            H0[0] = (double)1;
            H0[1] = (double)-1;
            bb = 2;
            N = (size_t)ny - 1;
            fin = 0;
        }
    else
        {
            for(i=0; i<tau-1; ++i)
                {
                    Tm = -tau+1+i;
                    Tp = i+1;
                    H0[i] = ((double)(-6*Tm*(Tm+tau))) / pow((double)tau, 3);
                    H0[i+tau-1] = ((double)(6*Tp*(Tp-tau))) / pow((double)tau, 3);
                    Tc[i] = Tm + tau - 1;
                    Tc[i+tau-1] = Tp + tau - 1;
                    bb = 2*tau - 2;
                    N = ny-(size_t)(2*tau + 1);
                    fin = 0;
                }
        }
    OV = 0;
    for (ia=0; ia<N; ++ia)
        {
            Ovi = 0;
            for(ja=0; ja<bb; ++ja)
                Ovi += H0[ja] * *(y+Tc[ja]+ia+fin);
            OV += Ovi * Ovi;
        }
    OV /= (double)(2*N);
    free(H0);
    free(Tc);
    return(sqrt(OV));
}


/**
 * \brief           Compute the deviation serie of a normalized frequency
 *                  deviation sequence.
 * \details         Deviation serie (ADEV, MDEV, PDEV or HDEV) of a normalized
 *                  frequency deviation sequence from tau=tau0 (tau0=sampling
 *                  step) to tau=N*tau0/2 or N*tau0/3 (N=number of samples)
 *                  according to the variance type (dev_type) by octave
 *                  (log step: tau_n+1=2*tau_n, default setting).
 *
 * \param *serie    Pointer to st_serie data structure.
 * \param dev_typ   Deviation type to compute (ADEV, MDEV, PDEV or HDEV).
 * \param ortau     Tau increment structure.
 *                  Choose 2 for increment by octave
 *                  (default setting), 10 for increment by decade, etc. If 1 is
 *                  selected, increasing values are expected, e.g. 'Tau: 1 2 5'.
 * \param n         Number of value.
 * \param tau       Integration time in second.
 * \param *y        Array of sample value.
 * \param ...       Another array of sample value.
 * \return          0 in case of successful completion.
 */
int st_serie_dev(st_serie *serie, st_tau_inc ortau, int dev_type, size_t n, double tau, double *y, ...)
{
    size_t i, j;
    int tomax, b, ndec, d0, psup, tn0, nto, indt, toi[256];
    double base, log_inc=2.;
    va_list ylist;
    double *y2;

    toi[0] = 1;
    if ((dev_type == MDEV) || (dev_type == HDEV))
        tomax = (int)n/3;
    else
        tomax = (int)n/2;
    // Construct 'toi[]'
    // Generation of Tau serie
    if ((ortau.tau[0] == (double)1)) //  && (ortau.length == 1))
        {
            i=0;
            do
                {
                 i++;
                 toi[i]=(int)(pow(log_inc,((double)i))*((double)toi[0]));
                }
                /* ORIGINAL
            b = 0;
            i = 0;
            do
            {
                base = (unsigned long)pow(10.0, (double)b);
                for (j=1; j<10; ++j)
                {
                    toi[i] = (unsigned long)(base * j);
                    i++;
                    if (toi[i-1] > tomax)
                    {
                        break;
                    }
                }
                */
            while(toi[i] < tomax);

            if (toi[i-1] < tomax)
                {
                    toi[i] = tomax;
                    nto = (int)i + 1;
                }
            else
                nto = (int)i;
        }
    else
        {
            ndec = (int)(log((double)(tomax-toi[0])) / log((double)10));
            d0 = (int)floor(log(((double)toi[0])*tau) / log(10));
            tn0 = (int)((double)toi[0] * tau / pow((double)10, (double)d0));
            psup = 0;
            for (i=1; i<=(size_t)ortau.length; ++i)
                if (ortau.tau[ortau.length-i] > tn0)
                    psup = (int)(ortau.length-i);
            if (!psup) ++d0;
            indt = 1;
            for (i=(size_t)psup; i<ortau.length; ++i)
                {
                    toi[indt] = (int)(ortau.tau[i] * pow((double)10, (double)d0) / tau);
                    if (toi[indt] == toi[indt-1]) ++toi[indt];
                    ++indt;
                }
            for (j=(size_t)(d0+1); j<(size_t)(d0+ndec+1); ++j)
                {
                    for(i=0; i<ortau.length; ++i)
                        {
                            toi[indt] = (int)(ortau.tau[i] * pow((double)10, (double)j) / tau);
                            ++indt;
                            if (toi[indt-1] >= tomax)
                                {
                                    toi[indt-1] = tomax;
                                    break;
                                }
                        }
                }
            nto = indt;
        }
    serie->length = (size_t)nto;
    
    va_start(ylist, y);
    
    for(i=0; i<serie->length; ++i)
        {
            serie->tau[i] = ((double)toi[i]) * tau;
            
            switch(dev_type)
                {
                case MDEV :
                    serie->dev[i] = mdev_y(y, toi[i], n);
                    break;
                case HDEV :
                    serie->dev[i] = hadamard_y(y, toi[i], n);
                    break;
                case PDEV :
                    serie->dev[i] = pdev_y(y, toi[i], n);
                    break;
                case GCOV :
                    y2 = va_arg(ylist, double*);
                    serie->dev[i] = gcodev_y(y,
                                             y2,
                                             toi[i], n);
                    break;
                default :
                    serie->dev[i] = adev_y(y, toi[i], n);
                }
            
        }
    va_end(ylist);
    return(0);
}
