/*   aduncert.c                                    F. Vernotte - 2010/12/31 */
/*   Computation of the uncertainties of a adev measurement serie           */
/*   without fitting nor plotting a graph                                   */
/*                                                                          */
/*                                                   - SIGMA-THETA Project  */
/*                                                                          */
/* Copyright or © or Copr. Université de Franche-Comté, Besançon, France    */
/* Contributor: François Vernotte, UTINAM/OSU THETA (2012/07/17)            */
/* Contact: francois.vernotte@obs-besancon.fr                               */
/*                                                                          */
/* This software, SigmaTheta, is a collection of computer programs for      */
/* time and frequency metrology.                                            */
/*                                                                          */
/* This software is governed by the CeCILL license under French law and     */
/* abiding by the rules of distribution of free software.  You can  use,    */
/* modify and/ or redistribute the software under the terms of the CeCILL   */
/* license as circulated by CEA, CNRS and INRIA at the following URL        */
/* "http://www.cecill.info".                                                */
/*                                                                          */
/* As a counterpart to the access to the source code and  rights to copy,   */
/* modify and redistribute granted by the license, users are provided only  */
/* with a limited warranty  and the software's author,  the holder of the   */
/* economic rights,  and the successive licensors  have only  limited       */
/* liability.                                                               */
/*                                                                          */
/* In this respect, the user's attention is drawn to the risks associated   */
/* with loading,  using,  modifying and/or developing or reproducing the    */
/* software by the user in light of its specific status of free software,   */
/* that may mean  that it is complicated to manipulate,  and  that  also    */
/* therefore means  that it is reserved for developers  and  experienced    */
/* professionals having in-depth computer knowledge. Users are therefore    */
/* encouraged to load and test the software's suitability as regards their  */
/* requirements in conditions enabling the security of their systems and/or */
/* data to be ensured and,  more generally, to use and operate it in the    */
/* same conditions as regards security.                                     */
/*                                                                          */
/* The fact that you are presently reading this means that you have had     */
/* knowledge of the CeCILL license and that you accept its terms.           */
/*                                                                          */
/*                                                                          */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "sigma_theta.h"

#define db(x) ((double)(x))
#define sisig(x) ((x) == 0) ? (db(0)) : (((x) > 0) ? (db(1)) : (db(-1)))

/**
 * \brief        Computes the 95 % (2 sigma) and the 68% (1 sigma) confidence
 *               intervals of a sequence of Allan Deviations.
 *
 * \param *serie  Pointer to st_serie data structure.
 * \param edf     Array of equivalent degree of freedom.
 * \return        5 asymptotes coefficients.
 */
int st_aduncert(st_serie *serie, double *edf)
{
  int i;
  conf_int srayl;

  for (i = 0; (size_t)i < (serie->length); ++i)
  {
    srayl = st_raylconfint(edf[i]);
    serie->conf_int[i].bmin2s = serie->dev[i] * sqrt(edf[i]) / srayl.sup_bound;
    serie->conf_int[i].bmax2s = serie->dev[i] * sqrt(edf[i]) / srayl.inf_bound;
    srayl = st_raylconfint1s(edf[i]);
    serie->conf_int[i].bmin1s = serie->dev[i] * sqrt(edf[i]) / srayl.sup_bound;
    serie->conf_int[i].bmax1s = serie->dev[i] * sqrt(edf[i]) / srayl.inf_bound;
    serie->dev_unb[i] = serie->dev[i] / srayl.mean;
  }

  return (0);
}
